"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

/*
 ************************SOLUCION 1:********************************
 CREO UNA COPIA DEL ARRAY DE OBJETOS A LA QUE AÑADO UNA NUEVA PROPIEDAD LLAMADA TOTALES EN LA QUE GUARDO EL TOTAL DE PUNTOS POR CADA EQUIPO
 ORDENO EL ARRAY DE OBJETOS SEGUN EL TOTAL DE PUNTOS POR CADA EQUIPO
 IMPRIMO LA PRIMERA Y ULTIMA POSICION DE ESTE ARRAY ORDENADO
 SI EL OBJETO NO TIENE LAS PROPIEDADES ADECUADAS, LANZO UN MENSAJE DE ERROR
 *******************************************************************
 */

/*

function checkProperties(objectList, property1, property2) {
  //FUNCION PARA COMPROBAR QUE EL OBJETO TIENE LAS PROPIEDADES NECESARIAS
  const hasCorrectProperty = (currentValue) =>
    currentValue.hasOwnProperty(property1) &&
    currentValue.hasOwnProperty(property2);

  return objectList.every(hasCorrectProperty);
}

function maxMinAnotaciones(puntuaciones) {
  if (checkProperties(puntuaciones, "equipo", "puntos")) {
    //CREO UNA COPIA DEL ARRAY DE OBJETOS A LA QUE AÑADO UNA NUEVA PROPIEDAD LLAMADA TOTALES EN LA QUE GUARDO EL TOTAL DE PUNTOS POR CADA EQUIPO
    let copiaPuntuaciones = JSON.parse(JSON.stringify(puntuaciones));

    copiaPuntuaciones = copiaPuntuaciones
      .map((equipo) => {
        equipo.totales = equipo.puntos.reduce(
          (sumador, puntos) => (sumador = sumador + puntos)
        );
        return equipo;
      })
      .sort((a, b) => {
        //ORDENO EL ARRAY DE OBJETOS SEGUN EL TOTAL DE PUNTOS POR CADA EQUIPO
        if (a.totales >= b.totales) {
          return +1;
        } else {
          return -1;
        }
      });

    //IMPRIMO LA PRIMERA Y ULTIMA POSICION DE ESTE ARRAY ORDENADO
    console.log(
      `El equipo con menos puntuación será ${copiaPuntuaciones[0].equipo} con una puntuacion de ${copiaPuntuaciones[0].totales} puntos totales`
    );

    console.log(
      `El equipo con más puntuación será ${
        copiaPuntuaciones[copiaPuntuaciones.length - 1].equipo
      } con una puntuacion de ${
        copiaPuntuaciones[copiaPuntuaciones.length - 1].totales
      } puntos totales`
    );
  } else {
    //SI EL OBJETO NO TIENE LAS PROPIEDADES ADECUADAS, LANZO UN MENSAJE DE ERROR
    throw "El objeto no dispone de las propiedades correctas";
  }
}

maxMinAnotaciones(puntuaciones);

/*
 *******************************SOLUCION 2*************************************************
 CREO UN ARRAY CON LAS PUNTUACIONES TOTALES DE CADA EQUIPO
 BUSCO LOS INDICES RELATIVOS A LAS PUNTUACIONES MAYORES Y MENORES DE ESTE ARRAY
 IMPRIMO LOS DATOS DE LOS EQUIPOS RELATIVOS A LOS INDICES MAYOR Y MENOR CALCULADOS
 SI EL OBJETO NO TIENE LAS PROPIEDADES ADECUADAS, LANZO UN MENSAJE DE ERROR
 ******************************************************************************************
 */

/*
function checkProperties(objectList, property1, property2) {
  //FUNCION PARA COMPROBAR QUE EL OBJETO TIENE LAS PROPIEDADES NECESARIAS
  const hasCorrectProperty = (currentValue) =>
    currentValue.hasOwnProperty(property1) &&
    currentValue.hasOwnProperty(property2);

  return objectList.every(hasCorrectProperty);
}

function maxMinAnotaciones(puntuaciones) {
  if (checkProperties(puntuaciones, "equipo", "puntos")) {
    let arrayPuntuacionesTotales = [];
    //CREO UN ARRAY CON LAS PUNTUACIONES TOTALES DE CADA EQUIPO
    let arrayPuntuaciones = puntuaciones.map((equipo) => equipo.puntos);
    for (const puntos of arrayPuntuaciones) {
      arrayPuntuacionesTotales.push(
        puntos.reduce((sumador, puntos) => (sumador = sumador + puntos))
      );
    }

    //BUSCO LOS INDICES RELATIVOS A LAS PUNTUACIONES MAYORES Y MENORES DE ESTE ARRAY
    let indexMax = arrayPuntuacionesTotales.indexOf(
      Math.max(...arrayPuntuacionesTotales)
    );
    let indexMin = arrayPuntuacionesTotales.indexOf(
      Math.min(...arrayPuntuacionesTotales)
    );

    //IMPRIMO LOS DATOS DE LOS EQUIPOS RELATIVOS A LOS INDICES MAYOR Y MENOR CALCULADOS
    console.log(
      `El equipo con menos puntuación será ${puntuaciones[indexMin].equipo} con una puntuacion de ${arrayPuntuacionesTotales[indexMin]} puntos totales`
    );
    console.log(
      `El equipo con mas puntuación será ${puntuaciones[indexMax].equipo} con una puntuacion de ${arrayPuntuacionesTotales[indexMax]} puntos totales`
    );
  } else {
    //SI EL OBJETO NO TIENE LAS PROPIEDADES ADECUADAS, LANZO UN MENSAJE DE ERROR
    throw "El objeto no dispone de las propiedades correctas";
  }
}

maxMinAnotaciones(puntuaciones);
*/
