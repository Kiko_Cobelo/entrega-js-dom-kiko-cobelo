"use strict";

function getRandomIntInclusive(min, max) {
  //FUNCIÓN PARA CREAR NÚMEROS ALEATORIOS ENTRE 2 VALORES (INCLUIDOS)
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let respuesta = 0;
let solucion = getRandomIntInclusive(0, 100);
let intentos = 5;

for (
  let intentosRestantes = intentos;
  intentosRestantes >= 0;
  intentosRestantes--
) {
  if (intentosRestantes === 0) {
    alert("GAME OVER");
  } else {
    respuesta = +prompt(
      `Adivine el numero (0-100). Intentos restantes: ${intentosRestantes}`
    );
    if (respuesta === solucion) {
      alert(`Su respuesta es correcta!`);
      break;
    } else if (respuesta < solucion) {
      alert(`Su resultado es menor que el resultado correcto.`);
    } else if (respuesta > solucion) {
      alert(`Su resultado es mayor que el resultado correcto.`);
    }
  }
}
