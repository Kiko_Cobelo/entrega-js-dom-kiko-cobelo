"use strict";

const formatNum = (num) => {
  //FUNCION AUXILIAR PARA CONVERTIR LOS VALORES DE UNA CIFRA EN DOS
  return num < 10 ? "0" + num : num;
};

let body = document.querySelector("body");

setInterval(() => {
  //GENERO UNA FECHA CADA SEGUNDO
  let fecha = new Date();
  //CONSTRUYO EL RELOJ MEDIANTE FUNCIONES DE FECHA
  let clock = `${formatNum(fecha.getHours())}:${formatNum(
    fecha.getMinutes()
  )}:${formatNum(fecha.getSeconds())}`;
  //MODIFICO EL HTML INTERNO DEL BODY PARA METER UN DIV CON EL RELOJ
  body.innerHTML = `
        <div>
            <h1 class="clock" style="text-align: center;
            margin-top: 200px;
            font-size: 100px;">${clock}</h1>
        </div>`;
}, 1000);
